import React, {Component} from 'react';
import dbContext from '../hoc-helper/db-hoc-context'


class Auth extends Component {

        state = {
            email: '',
            name: '',
            password: '',
            activeLink: 'home-tab'
        }


        onEmailChange = event => {
            this.setState({
                email: event.target.value
            })
        }
        onNameChange = event => {
            this.setState({
                name: event.target.value
            })
        }
        onPasswordChange = event => {
            this.setState({
                password: event.target.value
            })
        }
        loginHandler = async() => {
            const {email,password} = this.state;
            const login = await this.props.fetchData('http://localhost:5000/auth/login', 'POST', {email,password});
            console.log(login);
            this.setState({
                email: '',
                name: '',
                password: '',
            })
        }
        
        registerHandler = async() => {
            const {email,name,password} = this.state;
            await this.props.fetchData('http://localhost:5000/auth/registration', 'POST', {email,name,password});
            
        }
        onStyleChange = (e) => {
            
            this.setState({
                activeLink: e.target.id
            })
            console.log(e.target.className, e.target.id, this.state.activeLink);
            
        }
    render(){

       const classStyle = (this.state.activeLink === document.getElementById("home-tab"))? 'nav-link active': 'nav-link'
        
            return(
                
                
                <div className="col-7" style={{justifyContent: "center", alignContent: "center", marginTop: '10%'}}>
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item">
                        <a className='nav-link active' id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onClick={ this.onStyleChange}>Home</a>
                    </li>
                    <li className="nav-item">
                        <a className={classStyle} id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" onClick={this.onStyleChange}>Profile</a>
                    </li>
                    <li className="nav-item">
                        <a className={classStyle} id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false" onClick={this.onStyleChange}>Contact</a>
                    </li>
                    </ul>
                    <div className="tab-content" id="myTabContent">
                    <div className='container tab-pane active'id="home" role="tabpanel" aria-labelledby="home-tab">
                        
                        <div className="form-group" >
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value={this.state.email} onChange={this.onEmailChange}/>
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange}/>
                        </div>
                        <button className="btn btn-primary" onClick={this.loginHandler}>Submit</button>
                        </div>

                        
                    <div className='container tab-pane' id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    
                    
                        <div className="form-group" >
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value={this.state.email} onChange={this.onEmailChange}/>
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Name</label>
                            <input type="text" className="form-control" placeholder="Name" value={this.state.name} onChange={this.onNameChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange}/>
                        </div>
                        <button className="btn btn-primary" onClick={this.registerHandler}>Submit</button>
                        </div>
                
  <div className='container tab-pane ' id="contact" role="tabpanel" aria-labelledby="contact-tab"><p>faaaaaa</p></div>
</div>

</div>
            )
    }


}

export default dbContext(Auth);
import React from 'react'
import './spiner.css'

const Spiner = () =>{
    return (
        <div className="loadingio-spinner-double-ring-lw3ixclxdps spiner">
        <div className="ldio-uo957kyiv7">
        <div></div>
        <div></div>
        <div><div></div></div>
        <div><div></div></div>
        </div></div>
    )
}

export default Spiner;

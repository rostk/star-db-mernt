import React from 'react'
const withFunc = (fn) => (Wrapped) => {
    return (props) => {
      return (
        <Wrapped {...props}>
          {fn}
        </Wrapped>
      )
    };
  };
  
export default withFunc;
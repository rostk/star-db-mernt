import React from 'react';
import {FetchConsumer} from '../swapi-context/db-context'

const dbContext = (Wrapped) => {

    return (props) => {
      return (
        <FetchConsumer>
          {
            (fetchData) => {
              
              return (
                <Wrapped {...props} fetchData={fetchData}/>
              );
            }
          }
        </FetchConsumer>
      );
    }
  };
  
  export default dbContext;
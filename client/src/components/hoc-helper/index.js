
import withData from './with-data';
import swapiContext from './swapi-context';
import compose from './compose'
import withFunc from './withFun'
export {
  withData,
  swapiContext,
  compose,
  withFunc
};

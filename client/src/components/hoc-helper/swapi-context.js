import React from 'react';
import {Sconsumer} from '../swapi-context/swapi-context'

const swapiContext = (mapMethodsToProps) => (Wrapped) => {

    return (props) => {
      return (
        <Sconsumer>
          {
            (swapiService) => {
              const serviceProps = mapMethodsToProps(swapiService);
  
              return (
                <Wrapped {...props} {...serviceProps} />
              );
            }
          }
        </Sconsumer>
      );
    }
  };
  
  export default swapiContext;
 

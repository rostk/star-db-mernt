import React, {Component} from 'react';
import Spiner from '../spiner';

const withData = (ReactComponent)=> {
    return class extends Component{
        state = {
            data : null
        };

        componentDidMount(){
            this.props.getData()
                .then(data => this.setState({data}))
        }


        render(){
            const { data } = this.state;
            if(!data){
                return <Spiner />
            }

            return <ReactComponent data={data} {...this.props}/>
        }
    }
}

export default withData;
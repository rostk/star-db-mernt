import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Header from '../header';
import RandomPlanet from '../random-planet';
import { PeoplePage, PlanetPage, StarshipPage } from '../pages';
import SwapiService from '../../services/swapi-service'
import {Sprovider} from '../swapi-context/swapi-context';
import './app.css';
import { StarshipDetails } from '../swapi-components';
import Auth from '../auth';
import userRegister from '../../services/db-service';
import {FetchProvider} from '../swapi-context/db-context'
export default class App extends Component {
  swapiService = new SwapiService();
  
  isAuthenticated = false
  render (){

    if(!this.isAuthenticated){

      return <FetchProvider value={userRegister}><Auth /></FetchProvider>
    }
    return (
    <FetchProvider value={userRegister}>
      <Sprovider value={this.swapiService}>
      
      <BrowserRouter>
        <div>
          <Header />
          <RandomPlanet />
          <Switch>
            <Route path="/" render={()=> <h2>Hello</h2>} exact/>
            <Route path="/people/:id?" component={PeoplePage}/>
            <Route path="/planets" component={PlanetPage}/>
            <Route path="/starships" component={StarshipPage} exact/>
            <Route path="/starships/:id" render={({match, location, history})=>{
              const {id} = match.params;
              const {pathname} = location;
              return (
                <div>
                  <h2>{pathname}</h2>
              <StarshipDetails itemId={id} /> 
              </div>)
            }} />
            <Route render={()=> <h2>Page not found</h2>} />
            </Switch>
        </div>
    </BrowserRouter>
    </Sprovider>
    </FetchProvider>
    )
  }

    
  
};


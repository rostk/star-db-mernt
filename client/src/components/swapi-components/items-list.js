import React from 'react';
import ItemList from '../item-list';

import {withData,
        swapiContext,
        compose,
        withFunc} from '../hoc-helper';



const renderPeopleItems = (item) =>  (<span>{item.name} {item.birthYear}</span> )
const renderPlanetItems = (item) =>  (<span>{item.name} {item.rotationPeriod}</span> )
const renderStarshipItems = (item) =>  (<span>{item.name} {item.costInCredits}</span> )
const personMethod = (swapiService) => {
    return {
        getData : swapiService.getAllPeople
    }
}
const planetMethod = (swapiService) => {
    return {
        getData : swapiService.getAllPlanets
    }
}
const starshipMethod = (swapiService) => {
    return {
        getData : swapiService.getAllStarships
    }
}

const PeopleList =  compose(swapiContext(personMethod),
                        withData,
                        withFunc(renderPeopleItems)
                        )(ItemList);
                        
const PlanetList =  compose(swapiContext(planetMethod),
                            withData,
                            withFunc(renderPlanetItems)
                            )(ItemList);

const StarshipList =  compose(swapiContext(starshipMethod),
                              withData,
                              withFunc(renderStarshipItems)
                              )(ItemList);

export {
    PeopleList,
    PlanetList,
    StarshipList
}
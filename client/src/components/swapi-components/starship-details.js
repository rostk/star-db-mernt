import React from 'react';
import ItemDetails, {Record} from '../item-details/item-details';
import {swapiContext} from '../hoc-helper';



const StarshipDetails = (props) => {
    return (
      <ItemDetails {...props}>
        <Record field="model" label="Model" />
        <Record field="length" label="Length" />
        <Record field="costInCredits" label="Cost" />
      </ItemDetails>
    );
  };
  
  const mapMethodsToProps = (swapiService) => {
    return {
      getData: swapiService.getStarship,
      getImg: swapiService.getStarshipImage
    }
  };
  
  export default swapiContext(mapMethodsToProps)(StarshipDetails);
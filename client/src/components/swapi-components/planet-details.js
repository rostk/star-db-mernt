import React from 'react';
import ItemDetails, {Record} from '../item-details/item-details';
import {swapiContext} from '../hoc-helper';



const PlanetDetails = (props) => {
    return (
      <ItemDetails {...props}>
        <Record field="population" label="Population" />
        <Record field="rotationPeriod" label="Rotation Period" />
        <Record field="diameter" label="Diameter" />
      </ItemDetails>
    );
  };
  
  const mapMethodsToProps = (swapiService) => {
    return {
      getData: swapiService.getPlanet,
      getImg: swapiService.getPlanetImage
    };
  };
  
  export default swapiContext(mapMethodsToProps)(PlanetDetails);
import React from 'react';
import ItemDetails, {Record} from '../item-details/item-details';
import {swapiContext} from '../hoc-helper';

const PersonDetails = ({itemId, getData, getImg}) =>{
    
        return(
            <ItemDetails itemId={itemId}
                getData={getData}
                getImg={getImg}
                >
                <Record  label="Gender" field="gender"/>
                <Record  label="Birth Year" field="birthYear"/>
                <Record  label="Eye Color" field="eyeColor"/>
            </ItemDetails>
        )
   
}
const mapMethodsToProps = (swapiService) =>{
    return {
        getData: swapiService.getPerson,
        getImg: swapiService.getPersonImage
    }
}


export default swapiContext(mapMethodsToProps)(PersonDetails)
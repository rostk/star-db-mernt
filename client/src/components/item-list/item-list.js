import React from 'react';
import './item-list.css';

const  ItemList = (props) => {
  
  const {data, onItemSelect, children : renderItems} = props;

  const items = data.map((item)=>{
    const { id } = item;
    const list = renderItems(item);

    return (
      <li className="list-group-item"
          key={id}
          onClick={() => onItemSelect(id)}>
        {list}
      </li>
    );
  })


  return(
    <ul className="item-list list-group">
      {items}
    </ul>
  )
}

export default ItemList;
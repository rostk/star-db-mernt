import React, { Component } from 'react';


import './item-details.css';




const Record = ({item, label, field}) => {
  return (
    <li className="list-group-item">
            <span className="term">{label}</span>
            <span>{item[field]}</span>
          </li>
  )
}
export {
  Record
}

export default class ItemDetails extends Component {

  state = {
    item : null,
    image : null
  };

  onItemUpdate = (item) =>{
    this.setState({
      item,
      image : this.props.getImg(item)
    })
  }

  componentDidMount(){
    this.updateItem();
  }
  componentDidUpdate(prevId){
    const {itemId} = this.props;
    if(itemId !== prevId.itemId){
       this.updateItem();
    }
  }
  updateItem(){
    
    const {itemId, getData} = this.props;
  
    if(!itemId){
      return;
    }
      getData(itemId)
        .then(this.onItemUpdate)
      
  }

  


  render() {
    if(!this.state.item){
      return <span>Select someone from the list</span>
    }
    const { item, image } = this.state;
  
   
    return (
      <div className="person-details card">
        <img className="person-image"
          alt=""
          src={image} />

        <div className="card-body">
          <h4>{item.name}</h4>
          <ul className="list-group list-group-flush">
            {
              React.Children.map(this.props.children, (child)=>{
                return React.cloneElement(child, {item})
              })
            }
          </ul>
        </div>

      </div>
    )
  }
}



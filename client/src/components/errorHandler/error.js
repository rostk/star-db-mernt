import React from 'react';


import './error.css';
import icon from './death-star.png';

const ErrorHandler = () => {
    return (
        <div className="error">
            <img src={icon} alt=""/>
            <h3>404 Error</h3>
            <div>Something went wrong</div>
            <div>We have already send drons to fix your problem</div>
        </div>
    )
}

export default ErrorHandler;
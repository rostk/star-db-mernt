import React, { Component } from 'react';

import './random-planet.css';
import SwapiService from '../../services/swapi-service'
import Spiner from '../spiner'
import ErrorHandler from '../errorHandler';
export default class RandomPlanet extends Component {

  swapiService = new SwapiService();

  state = {
    planet : {},
    loading : true,
    error : false,
  }

  componentDidMount() {
    this.updatePlanet();
    this.interval = setInterval(this.updatePlanet, 10000);
  }
  componentWillUnmount(){
    clearInterval(this.interval);
  }
  onPlanetUpdate = (planet) =>{
    this.setState({planet, loading : false})
  }
  errorHandler =(err)=> {
    console.log(err); 
    this.setState({
      error : true,
      loading : false
    })
  }

  updatePlanet = () =>{
    console.log('update');
    const id = Math.floor(Math.random() * 20 + 1);
    this.swapiService
        .getPlanet(id)
        .then(this.onPlanetUpdate)
        .catch(this.errorHandler)

  }
  

  render() {
    const { planet, loading, error} = this.state;
   
   

    
    const hasData = !(loading || error);

    const load = loading ? <Spiner /> : null; 
    const onError = error ? <ErrorHandler /> : null;
    const content = hasData ? <PlanetView planet={planet}/> : null
    
    return (
      <div className="random-planet jumbotron rounded">
        {load}
        {content }
        {onError}
        </div>
        
    );
  }
}


const PlanetView = ({planet}) =>{
  const {id, name, population, rotationPeriod, diameter} = planet;
  return (
    <React.Fragment>
      <img className="planet-image" 
        src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt="" />

      <div>
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Population</span>
              <span>{population}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Rotation Period</span>
              <span>{rotationPeriod}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Diameter</span>
                <span>{diameter}</span>
            </li>
          </ul>
        </div>
    </React.Fragment>
  )
}
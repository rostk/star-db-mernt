import React, {Component} from 'react';
import Row from '../row';
import { PlanetList, PlanetDetails } from '../swapi-components';

export default class PlanetPage extends Component {
     
    state = {
        selectedPlanet : null
    };

    updatePlanet = (id) =>{
        this.setState({selectedPlanet : id})
    }

    render(){
        return (
            <Row left={<PlanetList onItemSelect = {this.updatePlanet}/>} 
            right={<PlanetDetails itemId={this.state.selectedPlanet}/>} />

        )
    }
}






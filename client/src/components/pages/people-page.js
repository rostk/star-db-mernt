import React from 'react';
import Row from '../row';
import {withRouter} from 'react-router-dom'
import { PeopleList, PersonDetails } from '../swapi-components';
const PeoplePage = ({match, history}) => {
    const {id} = match.params;
        return (
            <Row left={<PeopleList onItemSelect = {(id) => history.push(id)}/>}
             right={<PersonDetails itemId={id}/>} />
        )
    
}

export default withRouter(PeoplePage);
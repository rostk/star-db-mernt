import React from 'react';
import {withRouter} from 'react-router-dom'
import { StarshipList } from '../swapi-components';

const StarshipPage = ({ history }) => {

    return (
        <StarshipList onItemSelect={ (id) => history.push(id) }/>
    )
}

export default withRouter(StarshipPage);
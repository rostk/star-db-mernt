import React from 'react';

const { Provider  : Sprovider, Consumer : Sconsumer } =  React.createContext();

export { Sprovider, 
    Sconsumer };
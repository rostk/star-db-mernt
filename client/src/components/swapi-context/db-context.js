import React from 'react';

const { Provider  : FetchProvider, Consumer : FetchConsumer } =  React.createContext();

export { 
    FetchProvider,
    FetchConsumer 
     };


async function userRegister (url, method='POST', body = null, headers = {}) {
    try{
        if(body){
            body = JSON.stringify(body);
            headers['Content-Type'] = 'application/json'
        }
        const response = await fetch(url, {
            method,
            body,
            headers
        })
        const data = response.json();
        return data;
    }catch(e){
        throw e;
    }
}

export default userRegister;
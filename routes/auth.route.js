const {Router} = require('express');
const router = Router();
const bcrypt = require('bcryptjs')
const {check, validationResult} = require('express-validator')
const User = require('../models/user-model')
const jwt = require('jsonwebtoken')

router.post('/registration', [
    check('email', 'Incorrect email').isEmail(),
    check('password', ' Length of password should be more than 6 symbols')
      .isLength({ min: 6 })
  ],
  async (req, res) => {
  try {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
        message: 'Incorrect data'
      })
    }

        const {email, password, name} = req.body;
        console.log(email,password, name);
        const candidate = await User.findOne({email});

        if(candidate){
            res.status(400).json({message: 'incorrect data,try again'})
        }
        const hashPassword = await bcrypt.hash(password, 12);
        const user = new User({
            email,
            name,
            password: hashPassword
        })
        await user.save();
        res.status(201).json({message: 'User has been created'})
    }catch(e){
        res.status(500).json({ message: e.message })

    }
})

router.post('/login', [
    check('email', 'Data is incorrect').normalizeEmail().isEmail(),
    check('password', 'Enter a password').exists()
], async(req,res) =>{
  try{
    const {email, password} = req.body;

    const user = await User.findOne({email});

    if(!user) {
      res.status(400).json('Data is incorrect,try again');
    }
    const isPassword = bcrypt.compare(password, user.password);

    if(!isPassword){
      res.status(400).json('Data is incorrect,try again')
    }
    const token = jwt.sign(
      {userId: user.id},
      'rostyslav',
      {expiresIn: '1h'}
    )

    res.json({token, userId: user.id});
  }catch(e){
    res.status(500).json({message : e.message})
  }
});
  

  

module.exports = router;
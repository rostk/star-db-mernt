const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors()) 
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/auth', require('./routes/auth.route'));


const PORT = 5000;
async function start(){
    try{
        await mongoose.connect('mongodb+srv://rostyslav:0MvlPRrfEYMwp80T@cluster0-7qszp.mongodb.net/star-db',{useNewUrlParser : true, useUnifiedTopology: true, useFindAndModify: false});
        app.listen(PORT, ()=>console.log(`Server is live on port ${PORT}`));
    }catch(e){
        console.log(e);
        process.exit(1);
    }
    
}
start();